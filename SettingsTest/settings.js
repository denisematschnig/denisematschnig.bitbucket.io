/* -------------------- */
/*  Settings Functions  */
/* -------------------- */    

$(".settings").on("click", function(){
    $("#settings-modal").show("linear");
});

$("#closeSettings").on("click", function(){
        $("#settings-modal").hide("linear");
});

var selectedTheme;


$("#dinosaur").on("click", function(){
    selectedTheme = $("#dinosaur").attr("value");
    $(".selected-theme p").text(selectedTheme);
    console.log("Selected", selectedTheme);
});
$("#dark-mandala").on("click", function(){
    selectedTheme = $("#dark-mandala").attr("value");
    $(".selected-theme p").text(selectedTheme);
    console.log("Selected", selectedTheme);
});
$("#geometric").on("click", function(){
    selectedTheme = $("#geometric").attr("value");
    $(".selected-theme p").text(selectedTheme);
    console.log("Selected", selectedTheme);
});

$("#applyChanges").on("click", function(){
    if(selectedTheme === "Dinosaur"){
        $(".testbox").addClass("dinoTheme");
    }
});
