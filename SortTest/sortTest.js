let playerInformation = {};

playerInformation["a"] = 84;
playerInformation["s"] = 12;
playerInformation["d"] = 30;
playerInformation["f"] = 120;

let playerIDs = {};

playerIDs["a"] = "#player1";
playerIDs["s"] = "#player2";
playerIDs["d"] = "#player3";
playerIDs["f"] = "#player4";

counter = 1;

Object.keys(playerInformation).forEach(function (key)
{
    $(".playerInformation").append("<div id = 'player" + counter + "' class='endScore'><p class='name'>" + key + "</p><p class='score'>" + playerInformation[key] + "</p></div>");
    counter++;
});

var buffer = {};

//get smallest number
var smallest;
Object.keys(playerInformation).forEach(function(key){
    if(smallest == null){
        smallest = playerInformation[key];
    }
    else
    {
        if(playerInformation[key] < smallest)
        {
        smallest = playerInformation[key];
        }
    }
    
});

//get name of winner
function getKeyByValue(object, value) 
{
    return Object.keys(object).find(key => object[key] === value);
}
let winner = getKeyByValue(playerInformation, smallest);
console.log(winner);

$(playerIDs[winner]).addClass("winner");
