
$(document).ready(function(){
    
    var baseURL = "http://nowaunoweb.azurewebsites.net";

    let $topCard = $('.stacks .playedCards');
    let $draw = $(".newCards");
    let $colorPicker = $('#colorPicker');
    let game;
    let names = {};
    let direction = 1;
    let players = {};
    let playerIDs = {};
    let playerOrder;
    let currentPlayer;
    let playable = false;
    let colorPickerCards = [13, 14];
    let skipCards = [10,13];
    let reverseCard = [12];
    let plusCards = [10,13]; 
    let playerInformation = {};

    //Enter your name before the game begins
    //open window and make it uncloseable
    $("#playernames.modal").modal('show');
    $('#playernames.modal').modal({backdrop: 'static', keyboard: false});
    
    $('.nickname-form').on('keyup', function(event)
    {
        // if all playerNames are valid
        if(validatePlayerNames(event.currentTarget))
        {
            $('button', event.currentTarget).prop('disabled', !validatePlayerNames(event.currentTarget));
        }
    });
    

    $("#playerNamesSubmit").on('click', function(event)
    {
        // Prevent to switch to another url
        event.preventDefault();

        //get playerNames from form
        names = $('input:text').prop('disabled', true).map(function()
        {
            return this.value;
        }).get();

        // Start Game Request
        let request = $.ajax({
            url: baseURL + "/api/Game/Start",
            method: 'POST',
            data: JSON.stringify(names),
            contentType: 'application/json',
            dataType: 'json'
        });


        //Request done
        request.done(function(data)
        {
            console.log("StartGame Request", data);
            $('.modal').modal('hide');
            initializeGame(data);
        });
            

        //Request fail
        request.fail(function(msg)
        {
            console.log("Error in request ", msg);
        });
    });



    /* ---------------------- */
    /*  Validate Playernames  */
    /* ---------------------- */   
    let validatePlayerNames = function(form)
    {
        // ?
        let fields = $("input:text", form);
        let values = fields.map(function()
        {
            return this.value;
        }).filter(function()
        {
            return this.length > 0;
        }).get();
        return (new Set(values)).size == fields.length;
    };




    /* ----------------- */
    /*  Initialize Game  */
    /* ----------------- */   
    let initializeGame = function(data){
        //SpielID zuweisen
        game = data.Id;

        //Spielfeld zuweisen
        let board = $(".gameField");
        
        //if player draws a card
        $draw.on('click', function(event)
        {
            playable = false;
            $draw.prop('disabled', true);
            
            //DrawCard Request
            let request = $.ajax({
                url: baseURL + '/api/Game/DrawCard/' + game,
                method: 'PUT'
            });


            //Request done
            request.done(function(data){
                console.log("DrawCard Request", data);
                playable = true;
                $draw.prop('disabled', false);
                //create drawn card
                let $card = createPlayableCard(data.Card, data.Player);
                //append card to right player
                $(playerIDs[data.Player] + " .playerCards").append($card);
                playerInformation[data.Player] += data.Card.Score;
                $(playerIDs[data.Player] + " .points p").text(playerInformation[data.Player]);
                
                updateCurrentPlayer(data.NextPlayer);
            });


            //Request fail
            request.fail(function(msg){
                console.log('Error drawing card', msg);
            });
        });


        /*Display first card to play after == topcard*/
        let $displayCard = createCard(data.TopCard);
        $topCard.append($displayCard);
        $(".stacks .playedCards .back").hide();
       
        let counter = 1;
        data.Players.forEach(function(player)
        {
            //put in the initial score
            playerInformation[player.Player] = player.Score;
            playerIDs[player.Player] = "#player" + counter;
            let $player = createPlayer(player);
            players[player.Player] = $player;
            counter++;
        });
        

        playerOrder = data.Players.map(function(player)
        {
            return player.Player;
        });

        updateCurrentPlayer(data.NextPlayer);
        $(".gameField").removeClass('invisible');
        playable = true;
    };


    /* ----------------------- */
    /*  Update current Player  */
    /* ----------------------- */
    let updateCurrentPlayer = function(player, score)
    {
        if(player == currentPlayer)
        {
            console.log("Not updating current player, already set ", player);
            return;
        }

        currentPlayer = player;
        Object.keys(players).forEach(function(name)
        {
            let divloc = playerIDs[name] + " .playerCards";
            if(name === currentPlayer)
            {
                $(divloc).addClass('currentPlayerBorder');
                $(divloc + " .front").show();
                $(divloc + " .back").hide();

            }
            else{
                $(divloc).removeClass('currentPlayerBorder');
                $(divloc + " .back").show();
                $(divloc + " .front").hide();
            }
        });
    };
    
    /* --------------- */
    /*  Create Player  */
    /* --------------- */
    let createPlayer= function(player)
    {
        let $player = player.Player;
        let $score = player.Score;
       
        /*Insert the names at the right location*/ 
        Object.keys(playerIDs).forEach(function (key){
            $(playerIDs[$player] + " .playerName p").text($player);
            $(playerIDs[$player] + " .points p").text($score);

        });

        /*Insert cards*/
        player.Cards.forEach(function(card)
        {
            $playerCard = createPlayableCard(card, player.Player);
            Object.keys(playerIDs).forEach(function (key){
                $(playerIDs[player.Player] + " .playerCards").append($playerCard);
                $(playerIDs[player.Player] + " .card:first").addClass('first');
                $(playerIDs[player.Player] + " .card").hide().show('linear');
            });
        });
    };



    /* ------------------*/
    /*  Get Next Player  */
    /* ------------------*/
    let getNextPlayer = function()
    {
        let currentIndex = playerOrder.indexOf(currentPlayer);
        let newIndex = (currentIndex + direction) % playerOrder.length;
        if(newIndex < 0){
            newIndex += playerOrder.length;
        }
        return playerOrder[newIndex];
    };



    /* ---------------------- */
    /*  Create Playable Card  */
    /* ---------------------- */
    let createPlayableCard = function(card, player)
    {
        let $card = createCard(card);
        let p;
       
        $card.on('click', function(event)
        {
            if(player !== currentPlayer)
            {
                shakeCard(card);
                return;
            }
            if(!playable){
                return false;
            }
            if(colorPickerCards.includes(card.Value))
            {
                //if it was clicked before it would already have the class
                $("#colorPicker .close").removeClass("clicked");
                

                $("#colorPicker .close").click(function(){
                    $("#colorPicker .close").addClass("clicked");
                    $("#colorpicker").modal('hide');
                });


                $('button', $colorPicker).on('click', function(event)
                {
                    //removes eventhandlers
                    $('button', $colorPicker).off('click');
                    $colorPicker.modal('hide');

                    //if I havent clicked x the card is sent
                    let isClicked = $(".clicked");
                    if(isClicked.length === 0)
                    {
                        sendCard(card, $card, $(event.currentTarget).data('color'));
                    }
                });

                $colorPicker.modal('show');
            }
            else
            {
                sendCard(card, $card);
            }
        });
        return $card;
    };
    
    /* ----------- */
    /*  Send Card  */
    /* ----------- */   
    let sendCard = function(card, $card, wildColor, player)
    {
        let previousPlayer;
        $draw.prop('disabled', true);
        //to get the right id to update the playercards
        let playerThatPlayedCard = "#" + $card.parent().parent()[0].getAttribute('id');
        let p;
        

        console.log("Playerthatplayedcard", playerThatPlayedCard);

        Object.keys(playerIDs).forEach(function (key)
        {
            if(playerIDs[key] === playerThatPlayedCard)
            {
                p = key;
            } 
        });

        
        //send Request
        let url = baseURL + '/api/game/playCard/' + game + '?value=' + card.Value + '&color=' + card.Color + '&wildColor=';
        
        if(wildColor){
            url = url + wildColor;
        }
        
        let request = $.ajax({
            url: url,
            method: 'PUT',
            dataType: 'json'
        });


        //Request done
        request.done(function(data)
        {
            console.log("SendCard Request", data);
            $draw.prop('disabled', false);
            

            //If send card has an error
            if(data.hasOwnProperty('error'))
            {
                console.log('Error at playing card', data.error);
                shakeCard($card);
                return;
            }else{
                playerInformation[p] = playerInformation[p] - card.Score;
                $(playerIDs[p] + " .points p").text(playerInformation[p]);
            }
            
            //if played card is a colorpicker card
            if(colorPickerCards.includes(card.Value))
            {
                $topCard.empty();
                $topCard.append(createChosenWildCard(card, wildColor));    
            }else
            {
                $topCard.empty().append(createCard(card));
                $(".stacks .back").hide();
            }


            //if played card is a reverse card
            if(reverseCard.includes(card.Value))
            {
                direction = direction * -1;
            }
            
            //if played card is a card that skips a player
            if(skipCards.includes(card.Value))
            {
                updatePlayerCards(getNextPlayer());
            }


            //if played card is a card that gives a player cards
            if(plusCards.includes(card.Value)){
                updatePlayerCards(getNextPlayer());
            }

            //remove the played card from the player cards
            $card.remove();
            
            /* ----------- */
            /*  EndScreen  */
            /* ----------- */   
            if(data.Cards.length === 0)
            {
                $("#exitGame").click(function(){
                    //Works only in chrome
                    window.close();
                });

                $("#playAgain").on("click", function(event)
                {
                    location.reload();
                });

                console.log("Endscreen data", data);
                let $endScreen =  $("#endscreen");
                var counter = 1;

                Object.keys(playerInformation).forEach(function (key)
                {
                    $("#endscreen .modal-body").append("<div id = 'player" + counter + "' class='endScore'><p class='name'>" + key + "</p><p class='score'>" + playerInformation[key] + "</p></div>");
                    counter ++;
                });



                //get smallest number
                var smallest;
                Object.keys(playerInformation).forEach(function(key){
                    if(smallest == null){
                        smallest = playerInformation[key];
                    }
                else
                {
                    if(playerInformation[key] < smallest)
                    {
                        smallest = playerInformation[key];
                    }
                }
                
                });

                //get name of winner
                //return key that has the specific value
                function getKeyByValue(object, value) 
                {
                    return Object.keys(object).find(key => object[key] === value);
                }

                let winner = getKeyByValue(playerInformation, smallest);
                console.log(winner);

                $(playerIDs[winner] + ".endScore").addClass("winner");

                $(".gameField").addClass('invisible');
                $endScreen.show();
            }
            
            updateCurrentPlayer(data.Player, data.Score);
            
            Object.keys(playerIDs).forEach(function (key)
            {
                $(playerIDs[key] + " .card:first").addClass('first');
            });
            playable = true;
            
        });

            //Request fail
            request.fail(function(msg)
            {
                console.log("Server denied card", card, msg);
                shakeCard($card);
                $draw.prop('disabled', false);
            });
        };



        /* --------------------- */
        /*  Update Player Cards  */
        /* --------------------- */ 
        let updatePlayerCards = function(player)
        {
            //GetCards Request
            let request = $.ajax(
            {
                url: baseURL + '/api/Game/GetCards/' + game + '?playerName=' + player,
                method: 'GET',
                dataType: 'json'
            });
            
            //Request done
            request.done(function(data)
            {
                console.log("GetCards Request", data);
                playerInformation[data.Player] = data.Score;
                $(playerIDs[data.Player] + " .points p").text(playerInformation[data.Player]);
                let $deck = $(playerIDs[player] + " .playerCards");
                
                //remove all cards
                $deck.empty();

                //refill cards
                data.Cards.forEach(function(card) 
                {
                    let $card = createPlayableCard(card, player);
                    $deck.append($card);
                    $(playerIDs[player] + " .playerCards .front").hide();
                });

                $(playerIDs[player] + " .playerCards .card:first").addClass("first");

            });


            //request fail
            request.fail(function(msg)
            {
                console.error(msg);
            });
    };
    
    
        
        /* ----------- */
        /* Create Card */
        /* ----------- */
        let createCard = function(card)
        {
            let imgLocation = "<div class='card'><div class='front'><img src='cards_svg\\cards_svg\\";
            
            if(card.Value <= 9){
                if(card.Color == "Yellow"){
                    imgLocation += "j"; 
                }
                else if(card.Color == "Green"){
                    imgLocation += "v"; 
                }
                else{
                    imgLocation +=  card.Color.substring(0, 1).toLowerCase();
                }
                imgLocation += card.Value;
                imgLocation += ".svg";
            }
            
            else if(card.Value <= 12){
                if(card.Color == "Yellow"){
                    imgLocation += "j"; 
                }
                else if(card.Color == "Green"){
                    imgLocation += "v"; 
                }
                else{
                    imgLocation +=  card.Color.substring(0, 1).toLowerCase();
                }
                imgLocation += card.Text.toLowerCase() + ".svg"; 
            }
            
            else if(card.Value > 12){
                if(card.Text == 'Draw4'){
                    imgLocation += "wild4.svg"; 
                }
                else if(card.Text == 'ChangeColor'){
                    imgLocation += "wild.svg";
                }
            }
            imgLocation += "'></div><div class='back'><img src='cards_svg\\cards_svg\\back.svg'></div>";
            
            return $(imgLocation);
        };



        /* ------------------------ */
        /*  Create Chosen WildCard  */
        /* ------------------------ */
        let createChosenWildCard = function(card, color)
        {
            let imgLocation = "<div class='card'><img src='cards_svg\\cards_svg\\";
            if(card.Value == 13)
            {
                imgLocation += "wild4_";
                if(color == "Red")
                {
                    imgLocation += "r";
                }
                if (color == "Blue") 
                {
                    imgLocation += "b";
                }
                if (color == "Green") 
                {
                    imgLocation += "v";
                }
                if(color == "Yellow")
                {
                    imgLocation += "j";
                }
                
            }
            if(card.Value == 14){
                imgLocation += "wild_";
                if(color == "Red")
                {
                    imgLocation += "r";
                }
                if (color == "Blue") 
                {
                    imgLocation += "b";
                }
                if (color == "Green") 
                {
                    imgLocation += "v";
                }
                if(color == "Yellow")
                {
                    imgLocation += "j";
                }
            }
            imgLocation += ".svg'></div>"
            return $(imgLocation);
        };


    /* -------------------- */
    /*  Settings Functions  */
    /* -------------------- */    
    
    $(".settings").on("click", function(){
        $("#settings-modal").show("linear");
    });

    $("#closeSettings").on("click", function(){
            $("#settings-modal").hide("linear");
    });    

    var selectedTheme;
    var selectedBefore;


    $("#dinosaur").on("click", function(){
        //get theme value from chosen option
        selectedTheme = $("#dinosaur").attr("value");
        //write it on the screen 
        $(".selected-theme p").text("Dinos");
        //get the theme that is currently active
        selectedBefore = $('.gameField').attr('class').split(" ")[1];
    });
    $("#dark-mandala").on("click", function(){
        selectedTheme = $("#dark-mandala").attr("value");
        $(".selected-theme p").text("Mandala");
        selectedBefore = $('.gameField').attr('class').split(" ")[1];
    });
    $("#geometric").on("click", function(){
        selectedTheme = $("#geometric").attr("value");
        $(".selected-theme p").text("Geometric");
        selectedBefore = $('.gameField').attr('class').split(" ")[1];
    });

    $("#applyChanges").on("click", function()
    {
        //remove current theme
        $(".gameField").removeClass(selectedBefore);
        
        //add new theme
        if(selectedTheme === "dinoTheme")
        {
            $(".settings").empty().append("<img src='Maybe\\settings_black.png'>");
            $(".gameField").addClass("dinoTheme");
        }
        if(selectedTheme === "mandalaTheme")
        {
            $(".settings").empty().append("<img src='Maybe\\settings_white.png'>");
            $(".gameField").addClass("mandalaTheme");

        }
        if(selectedTheme === "geometricTheme")
        {
            $(".settings").empty().append("<img src='Maybe\\settings_black.png'>");
            $(".gameField").addClass("geometricTheme");
        }
        
    });

    /* ------------ */
    /*  Shake Card  */
    /* ------------ */
    let shakeCard = function(card)
    {
        $topCard.addClass('shake');
        setTimeout(function()
        {
            $topCard.removeClass('shake');
        }, 1000);
    };
    

    /* -------------------- */
    /*  Move Card on Hover  */
    /* -------------------- */
    $(document.body).delegate(".currentPlayerBorder.playerCards .card", "mouseenter", function()
    {
        $(this).animate(
            {
                top: '-15px'
            });
    });
        
    $(document.body).delegate(".currentPlayerBorder.playerCards .card", "mouseleave", function()
    {
        $(this).animate(
        {
            top: ''
        });
    });


})    